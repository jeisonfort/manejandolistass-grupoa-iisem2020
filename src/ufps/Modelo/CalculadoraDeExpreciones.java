/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Modelo;

import ufps.util.coleciones_seed.Pila;

/**
 *
 * @author jeiso
 */
public class CalculadoraDeExpreciones {

    public static char[] NUMEROS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    public static String NUM = "0123456789";
    public static String OP_MAYORES = "*/";
    public static String OP_MENORES = "+-p";
    protected static String OPERADORES = "^*/+-p";
    public static void main(String arg []) throws Exception{
        System.out.println("Posfijo: "+postfijo("(1+2-2/4^3)-2*3") );
        System.out.println("Prefijo: "+solPrefijo("(1+2-2/4^3)-2*3") );
    }
    public CalculadoraDeExpreciones(){
    }
    public static boolean verificardor(String cadena){
        return (evaluadorP(cadena)&& evaluadorOp(cadena));
    }
    private static boolean evaluadorOp(String cadena){
        Pila<Character> pila = new Pila();
        char ante = 'a';
        if( cadena.charAt(0)!='-'
                &&OPERADORES.contains(String.valueOf(cadena.charAt(0))))
            return false;
        for (char c : cadena.toCharArray()) {
            pila.push(c);
            if(OPERADORES.contains(String.valueOf(c))){
                if(c!='-'&&OPERADORES.contains(String.valueOf(ante)))return false;                            
            }         
            ante = c;
        }
        return true;
    }
    private static boolean evaluadorP(String cadena)
    { 
        int tamanio = -1;
        String c ;
        char cadenaC[] = cadena.toCharArray();
        Pila<String> pila = new Pila();
        while(++tamanio<cadena.length()){
            c=String.valueOf(cadenaC[tamanio]);
            if(c.equals("(")) pila.push(c);
            else if(c.equals(")") && pila.esVacia())
                return false;
                            
            else if(c.equals(")"))pila.pop();                   
        }
        return pila.esVacia();
    }
    /**
     * Soliciona una cadena en prefijo
     * @param cadena
     * @return
     * @throws Exception 
     */
    public static float solPrefijo(String cadena) throws Exception {

        if(verificardor(cadena)==false)throw new Exception("NO es una cadena bien formada");
        cadena = eliminarParentesis(preFijo(cadena));
        return solucionadorPre(cadena);
    }
    
    /**
     * Soluciona una expresión en posfijo
     * @param cadena
     * @return Solución a la expresión.
     * @throws Exception No está bien formada.
     */
    public static float postfijo(String cadena) throws Exception {
    
        if(verificardor(cadena)==false)throw new Exception("NO es una cadena bien formada");
        cadena = postFijo(cadena);
        cadena = eliminarParentesis(cadena);
        return solucionador(cadena);    
    }
    /**
     * Genera la cadena String en posfijo
     * @param cadena
     * @return posFijo : cadena en posfijo
     */
    public static String postFijo(String cadena) {
        String posFijo = "";
        char ant = 'k';
        Pila<Character> pila = new Pila();
        for (char c : cadena.toCharArray()) {
            if (OPERADORES.contains(String.valueOf(c)) || c == '(') {
                if (((c == '-' && posFijo.equals(""))
                        || (ant == '('
                        || OPERADORES.contains(String.valueOf(ant)))) && c == '-') {
                    posFijo += 'p';
                } else if ((pila.esVacia())
                        || (c == '^' || c == '(')
                        || (pila.peek() == '(')
                        || (OP_MAYORES.contains(String.valueOf(c))
                        && OP_MENORES.contains(String.valueOf(pila.peek())))) {
                    pila.push(c);
                } else {

                    posFijo += pila.pop();
                    pila.push(c);
                }

            } else if (c == ')') {
                while (!pila.esVacia() && pila.peek() != '(') {
                    posFijo += pila.pop();
                }
                posFijo += pila.pop();
            } else {
                posFijo += c;
            }

            ant = c;

        }
        while (!pila.esVacia()) {
            posFijo += pila.pop();
        }

        return posFijo;
    }
    /**
     * Retorna la cadena en forma de preFijo
     * @param cadena
     * @return resp: Respuesta con la cadena en prefijo
     */
    public static String preFijo(String cadena) {
        Pila<Character> op = new Pila();
        Pila<Character> cn = new Pila();
        Pila<Character> rest = new Pila();
        String resp = "";
        char caracteres[] = cadena.toCharArray();
        char c = 'f';
        char ant='l';
        for (int i = 0; i < caracteres.length; i++) {
            c = caracteres[i];
            if (NUM.contains(String.valueOf(c))) {
                cn.push(c);
            } else if (c == ')') {
                while (!op.esVacia() && op.peek() != '(') {
                    if (!cn.esVacia())rest.push(cn.pop());
                    if (!cn.esVacia()) {
                        rest.push(cn.pop());
                    }
                    if (!op.esVacia()) {
                        rest.push(op.pop());
                    }
                    if (!op.esVacia() && op.peek() == '(') {
                        op.pop();
                    }
                }

            } else {
                if (c == '-' &&(!NUM.contains(String.valueOf(ant)))
                        && ((c == '-' && rest.esVacia())
                        || (ant == '('
                        || OPERADORES.contains(String.valueOf(ant))))) {
                    op.push('p');
                } else if ((op.esVacia())
                        || (c == '^' || c == '(')
                        || (ant == '(')
                        || (OP_MAYORES.contains(String.valueOf(c))
                        && OP_MENORES.contains(String.valueOf(op.peek())))) {
                    op.push(c);
                } else {
                    rest.push(cn.pop());
                    if (!cn.esVacia()) {
                        rest.push(cn.pop());
                    }
                    if (!op.esVacia()) {
                        rest.push(op.pop());
                    }
                    op.push(c);
                }
            }
            ant = c;
        }
        while (!cn.esVacia()) {

            rest.push(cn.pop());
            if (!cn.esVacia()) {
                rest.push(cn.pop());
            }
            if (!op.esVacia()) {
                rest.push(op.pop());
            }
        }
       while (!op.esVacia()) {
            rest.push( op.pop());
        }
        while (!rest.esVacia()) {
            resp += rest.pop();
        }
         

        return resp;
    }
    /**
     * Soluciona una cadena en forma prefijo
     * @param prefijo
     * @return 
     * @throws Exception 
     */
    private static float solucionadorPre(String prefijo) throws Exception{
        Pila<Character> pila = new Pila();
        Pila<String> sol = new Pila();
        char carapter = 'l';
        String n1="";
        String n2 = "";
        boolean neg = false;
        for (char c : prefijo.toCharArray()) {
            pila.push(c);
        }
        while(!pila.esVacia()){
            carapter=pila.pop();
            if(OPERADORES.contains(String.valueOf(carapter))){
                if(carapter=='p'){
                    neg = true;               
                }
                else{
                    n1 = String.valueOf(sol.pop());
                    n2 = String.valueOf(sol.pop());
                    if(n1.contains(".")||Float.parseFloat(n1)<0||carapter=='^'||carapter=='/')sol.push(String.valueOf(operar(carapter, n1, n2)));
                    else sol.push(String.valueOf(operar(carapter, n2, n1)));
                }
            }else{
                 if (neg == true) {
                    n1 = String.valueOf(sol.pop());
                    sol.push(String.valueOf(operar('p', n1, "-1")));
                    neg = false;
                }
                sol.push(String.valueOf(carapter));
               
            }
            
        }
        return Float.parseFloat(sol.pop());
    }
    /**
     * Soluciona la cadena en forma posfijo
     * @param posFijo
     * @return
     * @throws Exception 
     */
    private static float solucionador(String posFijo) throws Exception {
        Pila<String> pila = new Pila();
        String n1 = "";
        String n2 = "";
        boolean neg = false;
        for (char c : posFijo.toCharArray()) {
            if (OPERADORES.contains(String.valueOf(c)) ) {
                if (c == 'p') {
                    neg = true;
                } else {
                    n1 = String.valueOf(pila.pop());
                    n2 = String.valueOf(pila.pop());
                    pila.push(String.valueOf(operar(c, n2, n1)));
                }
            } else {
                pila.push(String.valueOf(c));
                if (neg == true) {
                    n1 = String.valueOf(pila.pop());
                    pila.push(String.valueOf(operar('p', n1, "-1")));
                    neg = false;
                }
            }
        }
        return Float.parseFloat(pila.pop());

    }
    /**
     * Realiza las operaciones binarias
     * @param op operador
     * @param op1 primer operando
     * @param op2 segundo operando
     * @return respuesta
     * @throws Exception 
     */
    private static float operar(char op, String op1, String op2) throws Exception {
        try { 
            float n1 = Float.parseFloat(op1);
            float n2 = Float.parseFloat(op2);
            if (op == 'p') {
                op = '*';
            }
            switch (op) {
                case '+':
                    return n1 + n2;
                case '-':
                    return n1 - n2;
                case '*':
                    return n1 * n2;
                case '/':
                    return n1 / n2;
                case '^':
                    return (float) Math.pow((n1), (n2));
                default:
                    throw new Exception("EL OPERADOR NO ES CORRECTO");
            }
        } catch (Exception e) {
            throw new Exception("Es posible que algún valor no sea un número" + e.getMessage());
        }
    }
    /**
     * Elimina los parentesis en una cadena
     * @param cadena
     * @return cadena sin parentesis
     */
    public static String eliminarParentesis(String cadena) {
        String sinP = "";
        for (char c : cadena.toCharArray()) {
            if (c != '(' && c != ')') {
                sinP += c;
            }
        }
        return sinP;
    }
}
