/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import ufps.Modelo.Persona;
import ufps.util.coleciones_seed.ListaCD;
import ufps.util.coleciones_seed.ListaS;

/**
 *
 * @author jeiso
 */
public class TestGetPostListaCD {
    public static void main(String[] args) {
        System.out.println("Test con un solo elemento: ");
 ;
        ListaCD<Persona> persona=new ListaCD();
        persona.insertarInicio(new Persona(1,"madarme"));
        System.out.println(persona.toString());
        System.out.println(persona.get(0));
        
        System.out.println("******************************");
        int i=0;
        //Puebas lista Impar:
        System.out.println("Lista Tamaño Impar:");
        ListaCD<Persona> personas=new ListaCD();
        personas.insertarInicio(new Persona(1,"madarme"));
        personas.insertarInicio(new Persona(3,"gederson"));
        personas.insertarInicio(new Persona(4,"diana"));
        personas.insertarInicio(new Persona(5,"Jeison"));
        personas.insertarInicio(new Persona(6,"Gillermo"));
        System.out.println(personas);
        
        for (i=0 ; i < personas.getTamano();i++) {
            System.out.println("Persona en pos "+i+": "+personas.get(i));
        }
        
        System.out.println("******************************");
        ListaCD<Persona> personas2=new ListaCD();
        personas2.insertarFin(new Persona(1,"madarme"));
        personas2.insertarFin(new Persona(3,"gederson"));
        personas2.insertarFin(new Persona(4,"diana"));
        System.out.println(personas2);
        
        for (i=0 ; i < personas2.getTamano();i++) {
            System.out.println("Persona en pos "+i+": "+personas2.get(i));
        }
        
        System.out.println("******************************");
        //Pruebas lista tamaño par
        System.out.println("Lista Tamaño Par:");
        ListaCD<Persona> personas3=new ListaCD();
        personas3.insertarFin(new Persona(1,"madarme"));
        personas3.insertarFin(new Persona(3,"gederson"));
        
        System.out.println(personas3);
        for (i=0 ; i < personas3.getTamano();i++) {
            System.out.println("Persona en pos "+i+": "+personas3.get(i));
        }
        
        System.out.println("******************************");
        
        ListaCD<Persona> personas4=new ListaCD();
        personas4.insertarFin(new Persona(1,"madarme"));
        personas4.insertarFin(new Persona(3,"gederson"));
        personas4.insertarInicio(new Persona(4,"diana"));
        personas4.insertarInicio(new Persona(5,"Jeison"));
        System.out.println(personas4);
        
        for (i=0 ; i < personas4.getTamano();i++) {
            System.out.println("Persona en pos "+i+": "+personas4.get(i));
        }
        System.out.println("******************************");
        ListaCD<Integer> l=new ListaCD();
        int n=100000;
        for( i=0;i<=n;i++)
            l.insertarFin(i);
        System.out.println("Persona en pos "+99800+": "+l.get(99800));
    }
}
