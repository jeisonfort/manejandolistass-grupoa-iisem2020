/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import java.util.Scanner;
import ufps.util.coleciones_seed.Pila;

/**
 *
 * @author madar
 */
public class Evaluador_parentesis {
    
    public static void main(String[] args) {
        String cadena;
        System.out.println("Digite una cadena con ()");
        Scanner teclado = new Scanner(System.in);
        cadena=teclado.nextLine();
        System.out.println("Evaluando sus paréntesis "+evaluador(cadena));
        
    } 
    private static boolean evaluador(String cadena)
    { 
        // :)
        int tamanio = -1;
        String c ;
        char cadenaC[] = cadena.toCharArray();
        Pila<String> pila = new Pila();
        while(++tamanio<cadena.length()){
            c=String.valueOf(cadenaC[tamanio]);
            if(c.equals("(")) pila.push(c);
            else if(c.equals(")") && pila.esVacia())
                return false;
                            
            else if(c.equals(")"))pila.pop();                   
        }
        return pila.esVacia();
    }
    
    
}
