/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import ufps.Modelo.Persona;
import ufps.util.coleciones_seed.ListaCD;

/**
 *
 * @author jeiso
 */
public class TestPruebaPalindromo {
    public static void main (String arg []){
        System.out.println("Es palindromo para personas: ");
        
        ListaCD<Persona> lista1= new ListaCD();
        ListaCD<Persona> lista1p= new ListaCD();
        Persona juan = new Persona(0, "Juan");
        Persona pepe = new Persona(2, "Pepe");
        Persona juan2 = juan;
        Persona carlos = new Persona(0, "Carlos");
        
        //EL METODO EQUALS DE PERSONA SOLO COMPARA LAS CEDULAS
        lista1.insertarInicio(juan);
        lista1.insertarInicio(pepe);
        lista1.insertarInicio(juan2);
        
        
        //0 2 0
        System.out.println("Es palindromo l1: "+lista1.esPalindromo());
        
        lista1p.insertarInicio(juan);
        lista1p.insertarInicio(pepe);
        lista1p.insertarInicio(pepe);
        lista1p.insertarInicio(juan);
        
        //0 2 2 0
        System.out.println("Es palindromo l1p: "+lista1p.esPalindromo());
        
        lista1p.insertarInicio(carlos);
        //0 2 2 0 0
         System.out.println("Es palindromo l1p: "+lista1p.esPalindromo());
         
         
        System.out.println("***********************");
        System.out.println("Es palindromo para numeros: ");
        ListaCD<Integer> lista2 = new ListaCD<>();
        ListaCD<Integer> lista2p = new ListaCD<>();
        
        lista2.insertarInicio(1);
        lista2.insertarInicio(2);
        lista2.insertarInicio(3);
        System.out.println(lista2.esPalindromo());
        
        lista2p.insertarInicio(1);
        lista2p.insertarInicio(2);
        lista2p.insertarInicio(2);
        lista2p.insertarInicio(1);
        System.out.println(lista2p.esPalindromo());
        
        System.out.println("***********************");
        System.out.println("Es palindromo para letras: ");
        ListaCD<String> lista3 = new ListaCD<>();
        lista3.insertarInicio("a");
        lista3.insertarInicio("b");
        lista3.insertarInicio("a");
        System.out.println(lista3.esPalindromo());
        
        lista3.insertarInicio("d");
        System.out.println(lista3.esPalindromo());
        
        System.out.println("***********************");
        System.out.println("Es palindromo para lista vacia: ");
        ListaCD<String> lista4 = new ListaCD<>();
        System.out.println(lista4.esPalindromo());
        
        System.out.println("***********************");
        System.out.println("Es palindromo para lista con un elemento: ");
        lista4.insertarInicio("d");
        System.out.println(lista4.esPalindromo());
    }
}
