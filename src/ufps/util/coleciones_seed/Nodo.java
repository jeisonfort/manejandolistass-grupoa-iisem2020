/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.coleciones_seed;

/**
 *
 * @author madar
 */
class Nodo<T extends Comparable<T> > {
    
    private T info;
    private Nodo<T> sig;

    public Nodo() {
    }

    public Nodo(T info, Nodo<T> sig) {
        this.info = info;
        this.sig = sig;
    }

    public T getInfo() {
        return info;
    }

    public void setInfo(T info) {
        this.info = info;
    }

    public Nodo<T> getSig() {
        return sig;
    }

    public void setSig(Nodo<T> sig) {
        this.sig = sig;
    }
    
    
    
    
    
}
